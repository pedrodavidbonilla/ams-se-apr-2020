// 1. Store the Avengers data
// 2. Copy the data into the webpage
// 3. ul, p

console.log("Avengers Management System activated")

let roster = [
    ["Captain America", "Steve", 132],
    ["Hulk", "Bruce", 1500],
    ["Iron Man", "Tony", 200],
    ["Black Widow", "Natasha", 40],
    ["Hawkeye", "Clint", 0.1],
    ["Black Panther", "T'challa", 50],
    ["Scarlet Witch", "Wanda", 35],
    ["Antman", "Scott", 10],
]

// Get a reference to the <ul> so that we can put
// things into it later
let listEl = document.getElementById("avengers")

// Standard array loop
for (let heroIndex=0; heroIndex<roster.length; heroIndex++) {
    // Pull out the data from the array
    let heroName = roster[heroIndex][0]
    let realName = roster[heroIndex][1]
    let propertyDamage = roster[heroIndex][2]

    console.log(heroName)
    // Here's what we're going for:
    // <li>
    //     <h2>Captain America</h2>
    //     <p>Secret Identity: Steve<br>Property Damage: $132 million</p>
    // </li>

    // Create the <li>, <h2>, and text node
    let listItem = document.createElement("li")
    let heroHeader = document.createElement("h2")
    let textNode = document.createTextNode(heroName)

    // Put the text into the <h2>
    heroHeader.appendChild(textNode)
    // Put the <h2> into the <li>
    listItem.appendChild(heroHeader)
    
    // Using a different technique, put the secret identity
    // and property damage <p> into the <li>
    listItem.innerHTML = listItem.innerHTML + 
    "<p>Secret Identity: " + realName +
    "<br>Property Damage: $ " + propertyDamage + " million</p>"
    
    // Put the <li> into the <ul>
    listEl.appendChild(listItem)
}
